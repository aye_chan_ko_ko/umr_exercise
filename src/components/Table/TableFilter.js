import React from "react";
import Input from "../Form/Input/Input";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from "../Form/Input/Select";
import Button from "../Form/Button/Button";
import FormControl from "../Form/FormControl/FormControl";

function createSelectData(optionValue, optionText) {
  return { optionValue, optionText };
}

const searchTypeData = [
  createSelectData("name", "Name"),
  createSelectData("user_code", "User Code"),
  createSelectData("phone", "Phone Number"),
];

const TableFilter = ({ filteredData ,searchData ,setSearchData,searchTypeValue,setSearchTypeValue,handleSearchButton,loadFistData ,setPaginationShow}) => {

  const handleSearchChange= e =>{
    setSearchData(e.target.value);
    //console.log(e.target.value);
  //  { !(e.target.value) &&
  //     loadFistData();
  //     console.log("reload first page")
  //   }
    if(!e.target.value) {
      loadFistData();
      setPaginationShow(true)
    };
  }
  const handleSearchValue = e =>{
    setSearchTypeValue(e.target.value);
    //console.log(e.target.value);
  }
  return (
    <React.Fragment>
      <form className="md:flex md:justify-sktart">
        <div className="flex flex-wrap">
          <FormControl>
            <Button type="button">
              <FontAwesomeIcon
                icon="filter"
                className="md:mr-2"
              ></FontAwesomeIcon>
              <span className="inline">Action</span>
            </Button>
          </FormControl>
          <FormControl>
            <Button type="button">
              <FontAwesomeIcon
                icon="filter"
                className="md:mr-2"
              ></FontAwesomeIcon>
              <span className="inline">Filter</span>
            </Button>
          </FormControl>
        </div>
        <div className="flex">
          <FormControl className="w-auto">
            <Select
              className="w-32 sm:w-auto"
              optionData={searchTypeData}
              name="type"
              value={searchTypeValue}
              onChange={handleSearchValue}
            />
          </FormControl>
          <FormControl className="flex w-auto">
            <Input className="w-full" type="text" name="value"
            onChange={handleSearchChange} value={searchData} />
            <Button type="submit" className="ml-2" onClick={handleSearchButton}>
              <FontAwesomeIcon icon="search" />
            </Button>
          </FormControl>
        </div>
      </form>
    </React.Fragment>
  );
};

export default TableFilter;
