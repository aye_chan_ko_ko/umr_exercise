import React, { useState, useEffect, useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { TableActionContact } from "./TableActionContact";

const TableAction = ({
  //tableToggle,
  //setTableToggle,
  //indexNo,
  //setIndex,
  //index1,
  actionData,
  resultData,
  //setCurrentPage,
  //deactive,
  //setDeactive,
}) => {
  const [initialFieldValues, setInitialFieldValues] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    gender: "other",
    role_id: "",
    department_id: "",
    township_id: "",
    is_active: "",
  });
  const [initialPassChange, setInitialPassChange] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    gender: "other",
    role_id: "",
    department_id: "",
    township_id: "",
    is_active: "",
  });
  
  const auth = useContext(TableActionContact);
  //console.log(setIndex,'indexx')
  //console.log(indexNo ,index1)
  // if ((index1 === 0 || index1) && index1 === indexNo) {
  //   console.log(
  //     "outside Table",
  //     tableToggle,
  //     "tableToggle",
  //     index1,
  //     "indes",
  //     resultData.data[index1].id
  //   );
  // }
  useEffect(() => {
    if ((auth.index1 === 0 || auth.index1) && auth.index1 === auth.index) {
      axios
        .get(
          "https://api.picoehr.com/api/v1/users/" +
            `${resultData.data[auth.index1].id}`
        )
        .then((res) => {
          setInitialFieldValues({
            name: res.data.data.name,
            email: res.data.data.email,
            password: res.data.data.password,
            phone: res.data.data.phone,
            gender: res.data.data.gender,
            role_id: res.data.data.role_id,
            department_id: res.data.data.department_id,
            township_id: res.data.data.township_id,
            is_active: res.data.data.is_active === "true" ? "false" : "true",
          });
          setInitialPassChange({
            name: res.data.data.name,
            email: res.data.data.email,
            password: "$2y$10$SG8SMzI42x6zAa5TUoI8XeCGAfAEFBgGtZHt/k4gxrLUWZqW4GprG",
            phone: res.data.data.phone,
            gender: res.data.data.gender,
            role_id: res.data.data.role_id,
            department_id: res.data.data.department_id,
            township_id: res.data.data.township_id,
            is_active: res.data.data.is_active
          });
          auth.setCurrentPage(resultData.current_page);
        })
        .catch((err) => console.log(err));

      // console.log("axiosssss", index1, indexNo);
      // console.log(
      //   resultData.data[index1].email,
      //   "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
      // );
    }
  }, [auth.tableToggle, auth.deactive, resultData.data]);
  useEffect(() => {
    auth.setTableToggle(false);
  }, [resultData.data]);

  const handleDeactive = (e) => {
    //  setInitialFieldValues({
    //    ...initialFieldValues,is_active:"false"
    //  })
    auth.setTableToggle(!auth.tableToggle);
    axios
      .put(
        "https://api.picoehr.com/api/v1/users/" +
          `${resultData.data[auth.index1].id}`,
        initialFieldValues
      )
      .then((res) => {
        auth.setDeactive(!auth.deactive);
      })
      .catch((err) => console.log(err));
  };

  const handleResetPassword =e =>{
    auth.setTableToggle(!auth.tableToggle);
    axios
      .put(
        "https://api.picoehr.com/api/v1/users/" +
          `${resultData.data[auth.index1].id}`,
        initialPassChange
      )
      .then((res) => {
        alert("success reset Password.");
      })
      .catch((err) => console.log(err));
  }

  return (
    <div className="inline-flex flex-nowrap justify-between items-center">
      <div className="align-middle block md:inline-block p-2 absolute md:static top-0 right-0 mt-4 md:mt-0 mr-4 md:mr-0">
        <div className="relative">
          <div
            className="toggle-table-action"
            onClick={() => {
              auth.index1 === auth.index
                ? auth.setTableToggle(!auth.tableToggle)
                : auth.setTableToggle(true);
              auth.setIndex1(auth.index);
            }}
          >
            <FontAwesomeIcon
              className="text-xs text-gray-600 cursor-pointer hover:text-gray-900"
              icon="ellipsis-v"
            />
          </div>
          <div
            className={`bg-white shadow-lg rounded-lg absolute mt-4 top-0 right-0 py-4 text-sm font-bold text-gray-600 z-10 table-action ${
              !(auth.tableToggle && auth.index1 === auth.index) && "hidden"
            }`}
            // style={{ width: "250px" }}
          >
            <div className="py-2 pl-4 pr-6 flex items-center cursor-pointer text-sm font-bold hover:bg-green-100 hover:text-green-600">
              <div className="text-center flex-none mr-4 w-6">
                <FontAwesomeIcon className="text-md" icon="pencil-alt" />
              </div>
              <span className="flex-none">Edit</span>
            </div>
            <div className="py-2 pl-4 pr-6 flex items-center cursor-pointer text-sm font-bold hover:bg-green-100 hover:text-green-600">
              <div className="text-center flex-none mr-4 w-6">
                <FontAwesomeIcon
                  className="text-md"
                  icon={
                    initialFieldValues.is_active === "true"
                      ? "recycle"
                      : "trash"
                  }
                />
              </div>
              <span className="flex-none" onClick={handleDeactive}>
                {initialFieldValues.is_active === "true"
                  ? "Active"
                  : "Deactive"}
              </span>
            </div>
            <div className="py-2 pl-4 pr-6 flex items-center cursor-pointer text-sm font-bold hover:bg-green-100 hover:text-green-600">
              <div className="text-center flex-none mr-4 w-6">
                <FontAwesomeIcon className="text-md" icon="key" />
              </div>
              <span className="flex-none" onClick={handleResetPassword}>Reset</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TableAction;
