import React, { useState, useEffect } from "react";
import TableAction from "./TableAction";
import { act } from "react-dom/test-utils";
//import {useState} from 'react'
//import LoadingModal from "../../modals/LoadingModal";
//{{setIndex1,index1,deactive,setDeactive,index,tableToggle,setTableToggle,setCurrentPage}}

const TableDataRow = ({
  rowCount,
  tableDataCol,
  tableHeader,
  tableAction,
  resultData,
  actionCheck,
  setActionCheck,
}) => {
  //console.log("1122")
  const [singleCheck, setSingleCheck] = useState(false);
  const [noChangeCheck, setNoChangeCheck] = useState(false);
  const handleCheckChange = (e) => {
    //if(!singleCheck) setActionCheck(false);
    setSingleCheck(!singleCheck);
    if (actionCheck === false) {
      setNoChangeCheck(!noChangeCheck);
    }
  };
  useEffect(() => {
    setSingleCheck(false);
    setActionCheck(false);
    setNoChangeCheck(false);
  }, [resultData]);
  // useEffect(()=>{
  //   setSingleCheck(false)
  // },[actionCheck===false])
  useEffect(() => {
    if (actionCheck) {
      setSingleCheck(actionCheck);
    } else {
      setSingleCheck(actionCheck);
    }
  }, [actionCheck]);
  // useEffect(()=>{
  //   setSingleCheck(actionCheck);
  //   console.log("singleChangessssssssssssssssssss")
  // },[actionCheck])
  return (
    <div
      className={`relative p-4 mb-4 border bg-white rounded-lg shadow md:p-0 md:mb-0 md:bg-transparent md:rounded-none md:shadow-none md:border-none md:table-row ${
        rowCount % 2 === 0 ? "md:bg-gray-100" : ""
      }`}
    >
      <div className="hidden lg:table-cell md:px-2 md:py-3 lg:p-3 md:border-t">
        <input
          type="checkbox"
          checked={singleCheck}
          onChange={handleCheckChange}
        />
      </div>
      <div className="hidden lg:table-cell md:px-2 md:py-3 lg:p-3 md:border-t">
        {rowCount}
      </div>
      {tableDataCol.map((td, index) => (
        <React.Fragment key={index}>
          {index <= 3 && (
            <div className="block md:table-cell text-xs mb-1 md:text-sm md:px-2 md:py-3 lg:p-3 md:border-t">
              <span className="font-bold mr-3 text-gray-700 md:hidden">
                {tableHeader[index]}:
              </span>
              {td}
            </div>
          )}
          {/* {index > 3 && (
            <div className="block md:hidden lg:table-cell text-xs mb-1 md:text-sm md:px-2 md:py-3 lg:p-3 md:border-t">
              <span className="font-bold mr-3 text-gray-700 md:hidden">
                {tableHeader[index]}:
              </span>
              {td}
            </div>
          )} */}
          {index > 3 && td === "true" && (
            <div className="block md:hidden lg:table-cell text-xs mb-1 md:text-sm md:px-2 md:py-3 lg:p-3 md:border-t ">
              <span className="font-bold mr-3   font-bold text-gray-700 md:hidden">
                {tableHeader[index]}:
              </span>
              <span className=" text-green-600 bg-green-200   py-1 px-2 rounded-full text-xs font-bold">
                Active
              </span>
            </div>
          )}
          {index > 3 && td === "false" && (
            <div className="block md:hidden lg:table-cell text-xs mb-1 md:text-sm md:px-2 md:py-3 lg:p-3 md:border-t">
              <span className="font-bold mr-3 text-gray-700 md:hidden">
                {tableHeader[index]}:
              </span>
              <span className=" text-orange-600 bg-orange-200   py-1 px-2 rounded-full text-xs font-bold">
                Deactive
              </span>
            </div>
          )}
          {index > 3 && td !== "true" && td !== "false" && (
            <div className="block md:hidden bg-green:200 lg:table-cell text-xs mb-1 md:text-sm md:px-2 md:py-3 lg:p-3 md:border-t">
              <span className="font-bold mr-3 text-gray-700 md:hidden">
                {tableHeader[index]}:
              </span>
              {td}
            </div>
          )}
        </React.Fragment>
      ))}
      {tableAction && (
        <div className="block md:table-cell md:px-2 md:py-3 lg:p-3 text-right md:text-center md:border-t">
          <TableAction actionData={tableAction} resultData={resultData} />
        </div>
      )}
    </div>
  );
};

export default TableDataRow;
