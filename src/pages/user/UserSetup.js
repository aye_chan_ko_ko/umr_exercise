import React, { useState, useEffect } from "react";
import Topbar from "../../components/Topbar/Topbar";
import FormSection from "../../components/Form/FormWithSection/FormSection";
import FormControl from "../../components/Form/FormControl/FormControl";
import Input from "../../components/Form/Input/Input";
import Select from "../../components/Form/Input/Select";
import CheckBox from "../../components/Form/Input/Checkbox";
import FormFooter from "../../components/Form/FormFooter/FormFooter";
import Button from "../../components/Form/Button/Button";
import CSVSetup from "./CSVSetup";
//import axios from 'axios';
import axios from '../../axios_global';
import LoadingModal from '../../modals/LoadingModal'


const UserSetup = ({ toggleSidebar }) => {

  //SHOW AND HIDE DOCTOR AND STUDENT COMPONENT
  const [doctorForm,setDoctorForm] = useState(false);
  const [studentForm, setStudentForm] = useState(false);

  const handleDoctorCheck=e=>{
    setDoctorForm(!doctorForm);
  }
  const handleStudentCheck=e=>{
    setStudentForm(!studentForm);
  }

// START API DATA FETCHING FOR SHOW USERSETUP FORM
  const [department,setDepartment] = useState();
  const [role,SetRole] = useState();
  const [township,setTownship] = useState();
  const [education,setEducation] = useState();
  const [specialities,setSpecialities] = useState();
  const [designation,setDesignation] = useState();
  const [loading,setLoading] = useState(true);
  useEffect(()=>{
    setInterval(() => {
      setLoading(false);
    }, 3000);
    axios.get('/departments').then(response=>{
      setDepartment(response.data.data)
    })
    axios.get('/townships').then(response=>{
      setTownship(response.data.data)
    })
    axios.get('/roles').then(response=>{
      SetRole(response.data.data)
    })
    axios.get('/educations').then(response => {
      setEducation(response.data.data)
    })
    axios.get('/specialities').then(response => {
      setSpecialities(response.data.data)
    })
    axios.get('/designations').then(response => {
      setDesignation(response.data.data)
    })
  },[])
  //END API DATA FETCHING FOR SHOW USERSETUP FORM

  //START FIRST USERSET FORM POST TO API 
  const initialFieldValues = {
    name: "",  
    email:"",
    password: "" ,
    phone:"",
    gender:'other',
    role_id:'',
    department_id:'',
    township_id:''    
  };
  const [formValues, setFormValues] = useState(initialFieldValues);
  const [userNameError,setUserNameError] = useState('');
  const [emailError,setEmailError] = useState('');
  const [passwordError,setPasswordError] = useState('');
  const [phoneError,setPhoneError] = useState('');
  const [genderError,setGenderError] = useState('');
  const [roleError,setRoleError] = useState('');
  const [departmentError,setDepartmentError] = useState('');
  const [townshipError,setTownshipError] = useState('');
  //let reg=/^[a-zA-Z0-9]+@+[a-zA-Z0-9]+.+(com)/;
  const [successShow,setSuccessShow] = useState(false);
  const [apiError,setApiError] = useState({email:'',password:''})
  //END SETTING ERROR VALIDATION

  //START STUDENT FORM
  const initialStudentData = {
    roll_no:'',
    education_id:'',
    speciality_id:'',
    is_internship:"false",
    user_id:''
  }
  const [rollNoError,setRollNoError] = useState();
  const [educationError,setEducationError] = useState('');
  const [specialitiesError,setSpecialitiesError] = useState('');
  const [studentData,setStudentData] = useState (initialStudentData);
  const [successStudent,setSuccessStudent] = useState (false);
  const [checkInternship,setCheckInternship] = useState (false);
  const [studentApiError,setStudentApiError] = useState ({roll_no:'',user_id:''})


  //START DOCTOR FORM
  const initialDoctorData = {
    education_id:'',
    speciality_id:'',
    designation_id:'',
    user_id:''
  }
  const [doctorData,setDoctorData] = useState(initialDoctorData);
  const [educationError1,setEducationError1] = useState();
  const [specialitiesError1,setSpecialitiesError1] = useState();
  const [designationError,setDesignationError] = useState();
  const [successDoctor,setSuccessDoctor] = useState(false);
  const [doctorApiError,setDoctorApiError] = useState();

 //START FIRST USERSET FORM POST TO API "FUNCTION"
  const handleFormDataChange=e=>{
      var {name,value}=e.target
      setFormValues({
          ...formValues,
          [name]:value
      })              
  }
  const handleFormSave=e=>{
    e.preventDefault();
    (formValues.name) ? setUserNameError('') : setUserNameError("Please Fill User name");
    (formValues.email)? setEmailError('') : setEmailError("Please fill email");
    (formValues.password)? setPasswordError('') : setPasswordError("Please Fill password");
    (formValues.phone)? setPhoneError('') : setPhoneError("Please Fill correct Phone Number");
    (formValues.gender)? setGenderError('') : setGenderError ('please select gender');
    (formValues.role_id)? setRoleError('') : setRoleError ('Please select Role');
    (formValues.department_id)? setDepartmentError('') : setDepartmentError("please select department");
    (formValues.township_id)? setTownshipError('') : setTownshipError('Please select township');
    if(formValues.name && formValues.email && formValues.password && formValues.phone && formValues.gender && formValues.role_id && formValues.department_id && formValues.township_id){
      axios.post('/users',formValues).then(res=>{
        if(res.status===201){
          setSuccessShow(true);
          setFormValues(initialFieldValues);
          setStudentData({...studentData,user_id:res.data.data.id});
          setDoctorData({...doctorData,user_id:res.data.data.id});
          window.scrollTo({top:0,behavior:'smooth'})}
          // else{
          //   setSuccessShow(false)};
        setApiError({email:'',password:''})
      }).catch(error=>{
        setApiError({email:error.response.data.error.email,
          password:error.response.data.error.password});
          window.scrollTo({top:0,behavior:'smooth'})});
    }
    else{
      window.scrollTo({top:0,behavior:'smooth'})
      setSuccessShow(false)      
    }
  }
 //END FIRST USERSET FORM POST TO API "FUNCTION"

 //START STUDENT FORM POST TO API "FUNCTION"
  const handleStudentFormChange = e => {
    var {name,value}=e.target
    setStudentData({
        ...studentData,
        [name]:value
    }) 
    if(name==="is_internship")
    {setCheckInternship(e.target.checked);
    (e.target.checked ? setStudentData({
      ...studentData,
      is_internship:"true"
    }) :setStudentData({...studentData,is_internship:"false"}))}
  }

  const handleStudentFormSave = e => {
    e.preventDefault();
    (studentData.roll_no? setRollNoError(''):setRollNoError("Please fill Roll number"));
    (studentData.education_id? setEducationError(''):setEducationError("Please select education data."));
    (studentData.speciality_id? setSpecialitiesError(''):setSpecialitiesError("Please select specialities"));
    if(studentData.roll_no && studentData.education_id && studentData.speciality_id)
    {
      axios.post('/students',studentData).then(res=>{console.log(res,"studentDatasssss");
      setSuccessStudent(true);
      setCheckInternship(false);
      setStudentApiError({user_id:'',roll_no:''})
    setStudentData(initialStudentData)}).catch(err=>{console.log(err.response.data.error.user_id);
      setStudentApiError({user_id:err.response.data.error.user_id,roll_no:err.response.data.error.roll_no});
      setSuccessStudent(false)})
    }
    else{
      window.scrollTo({top:600,behavior:'smooth'})
      setSuccessStudent(false)      
    }
  }
//END STUDENT FORM POST TO API "FUNCTION"

//STRAT DOCTOR FORM POST TO API "FUNCTION"
  const handleDoctorFormChange = e =>{
    var {name,value}=e.target
    setDoctorData({
        ...doctorData,
        [name]:value
    }) 
  }
  const handleDoctorSave = e =>{
    e.preventDefault();
    (doctorData.education_id? setEducationError1(''):setEducationError1("Please select education"));
    (doctorData.speciality_id? setSpecialitiesError1(''):setSpecialitiesError1("please select specialities"));
    (doctorData.designation_id? setDesignationError(''):setDesignationError("please select designation"));
    if(doctorData.education_id && doctorData.speciality_id && doctorData.designation_id){
      axios.post('/doctors',doctorData).then(res=>{
        console.log(res,"doctorDataaaa");
        setSuccessDoctor(true);
        setDoctorData(initialDoctorData)
        setDoctorApiError('')
      }).catch(err=>{console.log(err,"errDoctor");
    setDoctorApiError(err.response.data.error.user_id)})
    }
    else{
      window.scrollTo({top:900,behavior:'smooth'})
      setSuccessStudent(false)      
    }
  }
  //STRAT DOCTOR FORM POST TO API "FUNCTION"

  const handleUpdateData=e=>{
    axios.put('users/4',)
  }
  return (
    <React.Fragment>
    <button onClick={handleUpdateData}>UpdateData</button>
      <Topbar toggleSidebar={toggleSidebar} pageTitle="User List" />
      {loading && <LoadingModal/>}
      {!loading && 
        <div className="my-1 px-4 lg:p-4">
        <div className="flex flex-wrap">
          <div className="w-full lg:w-3/5 lg:px-2">
            <div className="bg-white rounded-lg shadow-lg mb-4">
              <div className="p-4 border-b">
                <h1 className="text-lg">Create New User</h1>
              </div>
              <form>
                <FormSection sectionTitle="User Information">
                  {successShow? <span className='text-green-700 text-sm'>Successful</span>:null}
                  {apiError? <span className='text-red-700 text-sm'>{apiError.email}<br/>{apiError.password}</span>:null}
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Input
                      className="w-full"
                      type="text"
                      name="name"
                      labelText="User Name"
                      onChange={handleFormDataChange}
                      required={!userNameError? false:true}
                      error={userNameError}
                      value={formValues.name}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Input className="w-full"
                     type="email"
                     name="email"
                     labelText="Email"
                     required={!emailError? false:true}
                     value={formValues.email}
                     error={emailError}
                     onChange={handleFormDataChange} />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Input
                      className="w-full"
                      type="password"
                      name='password'
                      labelText="Password"
                      onChange={handleFormDataChange}
                      required={!passwordError? false : true}
                      error={passwordError}
                      value = {formValues.password}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Input
                      className="w-full"
                      type="text"
                      name="phone"
                      labelText="Phone Number"
                      onChange={handleFormDataChange}
                      required={!phoneError? false : true}
                      error={phoneError}
                      value = {formValues.phone}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Gender"
                      optionData={[
                        { optionValue: "male", optionText: "Male" },
                        { optionValue: "female", optionText: "Female" },
                      ]}
                      preViewData="Other"
                      className="w-full"
                      name="gender"
                      required={!genderError? false : true}
                      error={genderError}
                      value={formValues.gender}
                      onChange={handleFormDataChange}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Role"
                      // optionData={[
                      //   { optionValue: "patient", optionText: "Patient" },
                      //   { optionValue: "doctor", optionText: "Doctor" },
                      //   { optionValue: "student", optionText: "Student" },
                      // ]}
                      optionData1={role}
                      name="role_id"
                      className="w-full"
                      onChange={handleFormDataChange}
                      required={!roleError? false : true}
                      error={roleError}
                      value={formValues.role_id}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Department"
                      // optionData={[
                      //   {
                      //     optionValue: "in_patient_department",
                      //     optionText: "In patient Department",
                      //   },
                      //   {
                      //     optionValue: "out_patient_department",
                      //     optionText: "Out Patient Department",
                      //   },
                      // ]}
                      optionData1={department}
                      className="w-full"
                      name="department_id"
                      onChange={handleFormDataChange}
                      required={!departmentError ? false : true}
                      error = {departmentError}
                      value = {formValues.department_id}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Township"
                      // optionData={[
                      //   { optionValue: "mandalay", optionText: "Mandalay" },
                      //   { optionValue: "yangon", optionText: "Yangon" },
                      // ]}
                      optionData1={township}
                      className="w-full"
                      name="township_id"
                      onChange={handleFormDataChange}
                      required = {!townshipError ? false : true}
                      error = {townshipError}
                      value = {formValues.township_id}
                    />
                  </FormControl>
                  <FormFooter>
                    <Button className="ml-2" type="" onClick={handleFormSave}>
                      Save
                    </Button>
                  </FormFooter>
                </FormSection>
              </form>
              {successShow && 
                <div className="flex items-center justify-center p-2">
                <CheckBox name="student" labelText="Student" check={studentForm} onChange={handleStudentCheck}/>
                <CheckBox name="doctor" labelText="Doctor" check={doctorForm} onChange={handleDoctorCheck}/>
              </div>}
              {studentForm ? <form>
                <FormSection sectionTitle="Student Information">
                  {successStudent? <span className='text-green-700 text-sm'>Successful</span>:null}
                  {studentApiError? <span className='text-red-700 text-sm'>{studentApiError.user_id}<br/>{studentApiError.roll_no}</span>:null}
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Input
                      className="w-full"
                      type="text"
                      labelText="Roll Number"
                      name="roll_no"
                      onChange = {handleStudentFormChange}
                      required = {!rollNoError? false : true}
                      error = {rollNoError}
                      value = {studentData.roll_no}
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Educations"
                      // optionData={[
                      //   { optionValue: "master", optionText: "Master" },
                      //   { optionValue: "phd", optionText: "PHD" },
                      // ]}
                      optionData1={education}
                      onChange={handleStudentFormChange}
                      name="education_id"
                      required={!educationError ? false : true}
                      error = {educationError}
                      value = {studentData.education_id}
                      // preViewData="Other"
                      className="w-full"
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Specialities"
                      // optionData={[
                      //   { optionValue: "dental", optionText: "Dental" },
                      //   { optionValue: "liver", optionText: "Liver" },
                      //   { optionValue: "eye", optionText: "Eye" },
                      // ]}
                      optionData1={specialities}
                      name = "speciality_id"
                      onChange = {handleStudentFormChange}
                      required={!specialitiesError? false : true}
                      error = {specialitiesError}
                      value = {studentData.speciality_id}
                      className="w-full"
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <CheckBox name="student"
                     labelText="Internship"
                     name = "is_internship"
                     onChange = {handleStudentFormChange}
                     value = {checkInternship}
                     check = {checkInternship} />
                  </FormControl>
                  <FormFooter>
                    <Button className="ml-2" type="" onClick={handleStudentFormSave}>
                      Save
                    </Button>
                  </FormFooter>
                </FormSection>
              </form> :null}

              {doctorForm?<form>
                <FormSection sectionTitle="Doctor Information">
                {successDoctor? <span className='text-green-700 text-sm'>Successful</span>:null}
                {doctorApiError? <span className='text-red-700 text-sm'>{doctorApiError}</span>:null}
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Educations"
                      // optionData={[
                      //   { optionValue: "master", optionText: "Master" },
                      //   { optionValue: "phd", optionText: "PHD" },
                      // ]}
                      optionData1={education}
                      onChange={handleDoctorFormChange}
                      name="education_id"
                      required={!educationError1 ? false : true}
                      error = {educationError1}
                      value = {doctorData.education_id}
                      // preViewData="Other"
                      className="w-full"
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Specialities"
                      // optionData={[
                      //   { optionValue: "dental", optionText: "Dental" },
                      //   { optionValue: "liver", optionText: "Liver" },
                      //   { optionValue: "eye", optionText: "Eye" },
                      // ]}
                      optionData1={specialities}
                      onChange={handleDoctorFormChange}
                      name="speciality_id"
                      required={!specialitiesError1 ? false : true}
                      error = {specialitiesError1}
                      value = {doctorData.speciality_id}
                      className="w-full"
                    />
                  </FormControl>
                  <FormControl className="flex-grow md:flex-grow-0">
                    <Select
                      labelText="Designation"
                      // optionData={[
                      //   { optionValue: "dental", optionText: "Dental" },
                      //   { optionValue: "liver", optionText: "Liver" },
                      //   { optionValue: "eye", optionText: "Eye" },
                      // ]}
                      optionData1={designation}
                      onChange={handleDoctorFormChange}
                      name="designation_id"
                      required={!designationError ? false : true}
                      error = {designationError}
                      value = {doctorData.designation_id}
                      className="w-full"
                    />
                  </FormControl>
                  <FormFooter>
                    <Button className="ml-2" type="" onClick={handleDoctorSave}>
                      Save
                    </Button>
                  </FormFooter>
                </FormSection>
              </form>:null}
            </div>
          </div>
          <div className="w-full lg:w-2/5 lg:px-2">
            <CSVSetup />
          </div>
        </div>
      </div>
      }
    </React.Fragment>
  );
};

export default UserSetup;
